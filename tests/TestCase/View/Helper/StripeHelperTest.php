<?php
namespace Payment\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Payment\View\Helper\StripeHelper;

/**
 * Payment\View\Helper\StripeHelper Test Case
 */
class StripeHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Payment\View\Helper\StripeHelper
     */
    public $Stripe;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Stripe = new StripeHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Stripe);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
