<?php
namespace Payment\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Payment\Controller\PaymentsController;

/**
 * Payment\Controller\PaymentsController Test Case
 */
class PaymentsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.payment.payments'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
