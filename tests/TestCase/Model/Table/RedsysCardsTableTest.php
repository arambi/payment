<?php
namespace Payment\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Payment\Model\Table\RedsysCardsTable;

/**
 * Payment\Model\Table\RedsysCardsTable Test Case
 */
class RedsysCardsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Payment\Model\Table\RedsysCardsTable
     */
    public $RedsysCards;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Payment.RedsysCards',
        'plugin.Payment.Users',
        'plugin.Payment.Cards',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RedsysCards') ? [] : ['className' => RedsysCardsTable::class];
        $this->RedsysCards = TableRegistry::getTableLocator()->get('RedsysCards', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RedsysCards);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
