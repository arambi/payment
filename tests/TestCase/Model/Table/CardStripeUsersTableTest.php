<?php
namespace Payment\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Payment\Model\Table\CardStripeUsersTable;

/**
 * Payment\Model\Table\CardStripeUsersTable Test Case
 */
class CardStripeUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Payment\Model\Table\CardStripeUsersTable
     */
    public $CardStripeUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.payment.card_stripe_users',
        'plugin.payment.users',
        'plugin.payment.cards'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CardStripeUsers') ? [] : ['className' => CardStripeUsersTable::class];
        $this->CardStripeUsers = TableRegistry::get('CardStripeUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CardStripeUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
