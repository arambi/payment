<?php
namespace Payment\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Payment\Model\Table\PaymentsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Payment\Model\Table\PaymentsTable Test Case
 */
class PaymentsTableTest extends CrudTestCase
{

    /**
     * Test subject
     *
     * @var \Payment\Model\Table\PaymentsTable
     */
    public $Payments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.payment.payments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Payments') ? [] : ['className' => 'Payment\Model\Table\PaymentsTable'];
        $this->Payments = TableRegistry::get('Payments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Payments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Payments);
        $this->assertCrudDataIndex( 'index', $this->Payments);
      }
}
