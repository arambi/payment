<?php
namespace Payment\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Payment\Model\Table\StripeCardsTable;

/**
 * Payment\Model\Table\StripeCardsTable Test Case
 */
class StripeCardsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Payment\Model\Table\StripeCardsTable
     */
    public $StripeCards;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.payment.stripe_cards',
        'plugin.payment.users',
        'plugin.payment.cards'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StripeCards') ? [] : ['className' => StripeCardsTable::class];
        $this->StripeCards = TableRegistry::get('StripeCards', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StripeCards);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
