<?php
namespace Payment\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Payment\Model\Table\CardStripeOrdersTable;

/**
 * Payment\Model\Table\CardStripeOrdersTable Test Case
 */
class CardStripeOrdersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Payment\Model\Table\CardStripeOrdersTable
     */
    public $CardStripeOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.payment.card_stripe_orders',
        'plugin.payment.orders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CardStripeOrders') ? [] : ['className' => CardStripeOrdersTable::class];
        $this->CardStripeOrders = TableRegistry::get('CardStripeOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CardStripeOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
