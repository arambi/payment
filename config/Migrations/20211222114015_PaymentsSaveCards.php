<?php
use Migrations\AbstractMigration;

class PaymentsSaveCards extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payments')
            ->addColumn('save_card', 'boolean', ['null' => false, 'default' => 0])
            ->update();
    }
}
