<?php

use Migrations\AbstractMigration;

class RedsysCards extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payment_redsys_cards')
            ->addColumn('user_id', 'integer', ['default' => null, 'null' => true])
            ->addColumn('last4', 'string', ['default' => null, 'null' => true, 'limit' => 32])
            ->addColumn('exp_month', 'integer', ['default' => null, 'null' => true, 'limit' => 2])
            ->addColumn('exp_year', 'integer', ['default' => null, 'null' => true, 'limit' => 4])
            ->addColumn('card_id', 'string', ['default' => null, 'null' => true, 'limit' => 64])
            ->addColumn('brand', 'string', ['default' => null, 'null' => true, 'limit' => 64])
            ->addColumn('funding', 'string', ['default' => null, 'null' => true, 'limit' => 64])
            ->addColumn('country', 'string', ['default' => null, 'null' => true, 'limit' => 4])
            ->addColumn('default', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', array('default' => null))
            ->addColumn('modified', 'datetime', array('default' => null))
            ->addIndex(['user_id'])
            ->addIndex(['card_id'])
            ->create();
    }
}
