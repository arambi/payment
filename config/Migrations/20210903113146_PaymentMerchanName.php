<?php

use Migrations\AbstractMigration;

class PaymentMerchanName extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payments')
            ->addColumn('titular', 'string', ['null' => true, 'default' => null])
            ->update();
    }
}
