<?php

use Phinx\Migration\AbstractMigration;

class TransactionId extends AbstractMigration
{

  public function change()
  {
    $payments = $this->table( 'payments');
    $payments
        ->addColumn( 'transaction_id', 'string', ['null' => true, 'default' => null])
        ->addIndex( ['transaction_id'])
        ->save();
  }
}
