<?php
use Migrations\AbstractMigration;

class PaymentLocale extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $payments = $this->table( 'payments');
    $payments
        ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 8])
        ->update();
  }
}
