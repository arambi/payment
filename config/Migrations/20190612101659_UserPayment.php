<?php
use Migrations\AbstractMigration;

class UserPayment extends AbstractMigration
{

  public function change()
  {
    $users = $this->table( 'users');

    if( !$users->hasColumn( 'payment_id'))
    {
      $users
        ->addColumn( 'payment_id', 'integer', ['default' => null, 'null' => true])
        ->update();
    }
  }
}
