<?php
use Migrations\AbstractMigration;

class PaymentsSalt extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $payments = $this->table( 'payments')
      ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])
      ->update();
      
  }
}
