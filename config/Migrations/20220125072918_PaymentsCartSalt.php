<?php
use Migrations\AbstractMigration;

class PaymentsCartSalt extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payments')
            ->addColumn('card_salt', 'string', ['null' => true, 'default' => null])
            ->update();
    }
}
