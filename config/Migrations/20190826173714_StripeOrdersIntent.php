<?php
use Migrations\AbstractMigration;

class StripeOrdersIntent extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $payment_cards = $this->table( 'payment_cards_stripe_orders');
    $payment_cards
        ->addColumn( 'intent_id', 'string', ['default' => null, 'null' => true, 'limit' => 64])
        ->update();
  }
}
