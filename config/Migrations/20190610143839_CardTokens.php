<?php
use Migrations\AbstractMigration;

class CardTokens extends AbstractMigration
{

  public function change()
  {
    $payment_cards = $this->table( 'payment_cards_stripe_orders');
    $payment_cards
        ->addColumn( 'order_id', 'integer', ['default' => null, 'null' => true])
        ->addColumn( 'token', 'string', ['default' => null, 'null' => true, 'limit' => 64])
        ->addColumn( 'customer_id', 'string', ['default' => null, 'null' => true, 'limit' => 64])
        ->addColumn( 'card_id', 'string', ['default' => null, 'null' => true, 'limit' => 64])
        ->addColumn( 'model', 'string', ['null' => true, 'default' => null, 'limit' => 32])
        ->addColumn( 'save', 'boolean', ['null' => false, 'default' => 0])
        ->addColumn( 'created', 'datetime', array('default' => null))
        ->addColumn( 'modified', 'datetime', array('default' => null))
        ->addIndex( ['order_id'])
        ->addIndex( ['token'])
        ->addIndex( ['card_id'])
        ->create();

    $users = $this->table( 'users');

    if( !$users->hasColumn( 'stripe_customer_id'))
    {
      $users
        ->addColumn( 'stripe_customer_id', 'string', ['default' => null, 'null' => true, 'limit' => 64])
        ->update();
    }

  }
}
