<?php

use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{

  public function change()
  {
    $payments = $this->table( 'payments');
    $payments
        ->addColumn( 'order_id', 'string', ['default' => null, 'null' => true])
        ->addColumn( 'request', 'text', ['default' => null, 'null' => true])
        ->addColumn( 'response', 'text', ['default' => null, 'null' => true])
        ->addColumn( 'model', 'string', ['null' => true, 'default' => null, 'limit' => 32])
        ->addColumn( 'callback', 'string', ['null' => true, 'default' => null, 'limit' => 32])
        ->addColumn( 'provider', 'string', ['null' => true, 'default' => null, 'limit' => 32])
        ->addColumn( 'amount', 'float', ['null' => true, 'default' => NULL])
        ->addColumn( 'success', 'boolean', ['null' => true, 'default' => 0])
        ->addColumn( 'sent_at', 'datetime', array('default' => null))
        ->addColumn( 'response_at', 'datetime', array('default' => null))
        ->addColumn( 'created', 'datetime', array('default' => null))
        ->addColumn( 'modified', 'datetime', array('default' => null))
        ->addIndex( ['model'])
        ->addIndex( ['order_id'])
        ->addIndex( ['success'])
        ->save();
  }
}
