<?php

use Migrations\AbstractMigration;

class DefaultValues extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payments')
            ->changeColumn('sent_at', 'datetime', array('default' => null, 'null' => true))
            ->changeColumn('response_at', 'datetime', array('default' => null, 'null' => true))
            ->update();
    }
}
