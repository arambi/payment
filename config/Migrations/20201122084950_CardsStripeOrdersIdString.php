<?php

use Migrations\AbstractMigration;

class CardsStripeOrdersIdString extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payment_cards_stripe_orders')
            ->changeColumn('order_id', 'string', ['default' => null, 'null' => true])
            ->update();
    }
}
