<?php 
use Section\Action\ActionCollection;
 
use Manager\Navigation\NavigationCollection;
 
use User\Auth\Access;
 




Access::add( 'payments', [
  'name' => 'Pagos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Payment',
          'controller' => 'Payments',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Payment',
          'controller' => 'Payments',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Payment',
          'controller' => 'Payments',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Payment',
          'controller' => 'Payments',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Payment',
          'controller' => 'Payments',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Payment',
          'controller' => 'Payments',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Payment',
          'controller' => 'Payments',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Pagos',
  'parentName' => 'Pagos',
  'plugin' => 'Payment',
  'controller' => 'Payments',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);



ActionCollection::set( 'payments', [
  'label' => 'Pagos',
  'plugin' => 'Payment',
  'controller' => 'Payments',
  'action' => 'index',
  'icon' => 'fa fa-square',
  'actions' => [
    'view' => '/:slug'
  ]
]);