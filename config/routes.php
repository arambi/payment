<?php
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Route\DashedRoute;

if( Configure::version() > '3.8.6')
{
  Router::plugin( 'Payment', [
    'path' => '/{lang}/payment'
  ], function (RouteBuilder $routes) {
      $routes->fallbacks(DashedRoute::class);
    }
  );
}
else
{
  Router::plugin( 'Payment', [
    'path' => '/:lang/payment'
  ], function (RouteBuilder $routes) {
      $routes->fallbacks(DashedRoute::class);
    }
  );
}

Router::plugin(
    'Payment',
    ['path' => '/payment'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);