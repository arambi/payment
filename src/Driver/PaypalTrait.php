<?php 

namespace Payment\Driver;

use Cake\Core\Configure;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

trait PaypalTrait
{
  public function apiContext()
  {
    $keys = $this->getKeys();
    $apiContext = new ApiContext(
        new OAuthTokenCredential(
            $keys ['id'],
            $keys ['secret']
        )
    );

    $apiContext->setConfig(
        array(
            'mode' => Configure::read( 'App.payEnvironment') == 'development' ? 'sandbox' : 'live',
            'log.LogEnabled' => true,
            'log.FileName' => ROOT . DS. 'logs' .DS. 'PayPal.log',
            'log.LogLevel' => 'DEBUG', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
            'cache.enabled' => true,
            // 'http.CURLOPT_CONNECTTIMEOUT' => 30
            // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
            //'log.AdapterFactory' => '\PayPal\Log\DefaultLogFactory' // Factory class implementing \PayPal\Log\PayPalLogFactory
        )
    );

    return $apiContext;
  }

  public function getKeys()
  {
    $env = Configure::read( 'App.payEnvironment') == 'development' ? 'development' : 'production';
    $settings = Configure::read( 'Payment.paypal')['data'][$env];

    if( !is_array( $settings) || empty( $settings ['id']) || empty( $settings ['secret']))
    {
      throw new \RuntimeException(
          'No está configurado Paypal'
      );
    }

    return [
      'id' => $settings ['id'],
      'secret' => $settings ['secret']
    ];
  }
}