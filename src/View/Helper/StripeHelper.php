<?php

namespace Payment\View\Helper;

use Stripe\Stripe;
use Cake\View\View;
use Stripe\Customer;
use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Store\Error\StripeErrors;

/**
 * Stripe helper
 */
class StripeHelper extends Helper
{
    public $helpers = array('Html', 'Form', 'User.Auth');

    public $events = [
        'onsubmit' => '',
        'onerror' => '',
        'onsuccess' => ''
    ];

    public function getUserCards()
    {
        $keys = $this->getKeys();
        Stripe::setApiKey($keys['secret']);

        if (!$this->Auth->user()) {
            return;
        }

        $cards = TableRegistry::get('Payment.StripeCards')->find()
            ->where([
                'StripeCards.user_id' => $this->Auth->user('id')
            ]);

        return $cards;
    }

    public function getDefaultCard($customer)
    {
        if ($customer->default_card) {
            foreach ($customer->cards->data as $card) {
                if ($card->id == $customer->default_card) {
                    return $card;
                }
            }
        }
    }


    public function getKeys()
    {
        $prefix = Configure::read('App.payEnvironment') == 'development' ? 'test' : 'live';
        $config = Configure::read('Stripe.' . $prefix);

        if (!$config) {
            throw new \RuntimeException(
                'No está configurado Stripe'
            );
        }

        return [
            'public' => $config['public'],
            'secret' => $config['secret'],
        ];
    }

    public function getKey($name)
    {
        return $this->getKeys()[$name];
    }

    public function createIntent($order_id, $amount, $model)
    {
        \Stripe\Stripe::setApiKey($this->getKeys()['secret']);

        $data = [
            'amount' => round($amount * 100),
            'currency' => 'eur',
            'metadata' => [
                'Pedido' => $order_id
            ]
        ];

        if ($this->Auth->user()) {
            $user = TableRegistry::getTableLocator()->get('User.Users')->find()
                ->where([
                    'Users.id' => $this->Auth->user('id')
                ])
                ->first();

            if ($user->stripe_customer_id) {
                $data['customer'] = $user->stripe_customer_id;
            }
        }


        $customer_id = $this->Auth->user('stripe_customer_id');

        $CardStripeOrders = TableRegistry::getTableLocator()->get('Payment.CardStripeOrders');

        $entity = $CardStripeOrders->find()
            ->where([
                'order_id' => $order_id,
                'model' => $model,
            ])
            ->first();

        if (!$entity) {
            $entity = $CardStripeOrders->newEntity([
                'order_id' => $order_id,
                'model' => $model
            ]);
        }

        if (!empty($entity->intent_id)) {
            try {
                $intent = \Stripe\PaymentIntent::retrieve($entity->intent_id);
            } catch (\Throwable $th) {
            }
        }

        if (!isset($intent)) {
            $intent = \Stripe\PaymentIntent::create($data);
        }

        $entity->set('intent_id', $intent->id);
        $CardStripeOrders->save($entity);
        return $intent;
    }
}
