
  <script type="text/javascript" charset="utf-8">
    window.onload = function() { document.getElementById( "payment_form").submit() }
  </script>
<?php 
if(!isset($data['form_method'])) {
  $data['form_method'] = 'post';
}
?>
<?# _d( $data)?>
<form action="<?php echo $data['form_action']; ?>" id="payment_form" method="<?php echo $data['form_method'] ?>">
<?php foreach($data as $key => $value) {?>
    <input name="<?php echo $key; ?>" type="hidden" value="<?php echo $value; ?>" />
<?php } ?>
<noscript>
<button type="submit"><?php __('Send'); ?></button>
</noscript>
<div class="payment_loading">
  <?= __( "Conectando") ?>
</div>

</form>