<?php
namespace Payment\Model\Entity;

use Cake\ORM\Entity;

/**
 * RedsysCard Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $last4
 * @property int|null $exp_month
 * @property int|null $exp_year
 * @property string|null $card_id
 * @property string|null $brand
 * @property string|null $funding
 * @property string|null $country
 * @property bool $default
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \Payment\Model\Entity\User $user
 * @property \Payment\Model\Entity\Card $card
 */
class RedsysCard extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'last4' => true,
        'exp_month' => true,
        'exp_year' => true,
        'card_id' => true,
        'brand' => true,
        'funding' => true,
        'country' => true,
        'default' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'card' => true,
    ];
}
