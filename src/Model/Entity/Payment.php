<?php
namespace Payment\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;use Cake\ORM\Entity;

/**
 * Payment Entity
 *
 * @property int $id
 * @property string $order_id
 * @property string $data
 * @property string $model
 * @property string $callback
 * @property string $provider
 * @property float $amount
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Payment extends Entity
{
  use CrudEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'id' => false
  ];

  protected function _getResponseCode()
  {
    if( $this->provider == 'redsys' && !empty( $this->response))
    {
      try {
        $Redsys = new \Payment\Driver\Redsys();
        $json = $Redsys->decodeMerchantParameters( $this->response->Ds_MerchantParameters);
        $data = json_decode( $json);
        return 'Respuesta del TPV: '. $data->Ds_Response;
      } catch (\Throwable $th) {
        //throw $th;
      }
    }
    
    return '-';
  }
}
