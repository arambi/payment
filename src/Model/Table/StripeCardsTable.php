<?php
namespace Payment\Model\Table;

use Cake\ORM\Table;

class StripeCardsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('payment_stripe_cards');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');

    $this->addBehavior('Timestamp');

    $this->belongsTo('Users', [
        'foreignKey' => 'user_id',
        'className' => 'User.Users'
    ]);

  }

}
