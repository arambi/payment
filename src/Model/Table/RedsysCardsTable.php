<?php

namespace Payment\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RedsysCards Model
 *
 * @property \Payment\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \Payment\Model\Table\CardsTable&\Cake\ORM\Association\BelongsTo $Cards
 *
 * @method \Payment\Model\Entity\RedsysCard get($primaryKey, $options = [])
 * @method \Payment\Model\Entity\RedsysCard newEntity($data = null, array $options = [])
 * @method \Payment\Model\Entity\RedsysCard[] newEntities(array $data, array $options = [])
 * @method \Payment\Model\Entity\RedsysCard|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Payment\Model\Entity\RedsysCard saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Payment\Model\Entity\RedsysCard patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Payment\Model\Entity\RedsysCard[] patchEntities($entities, array $data, array $options = [])
 * @method \Payment\Model\Entity\RedsysCard findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RedsysCardsTable extends Table
{

    public static $brands = [
        1 => 'VISA',
        2 => 'MASTERCARD',
        6 => 'DINERS',
        7 => 'PRIVADA',
        8 => 'AMEX',
        9 => 'JCB',
        22 => 'UPI'
    ];


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payment_redsys_cards');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'Payment.Users',
        ]);
        $this->belongsTo('Cards', [
            'foreignKey' => 'card_id',
            'className' => 'Payment.Cards',
        ]);
    }

    public function getUserCards($user_id)
    {
        if (empty($user_id)) {
            return [];
        }
        
        $cards = $this->find()
            ->where([
                'RedsysCards.user_id' => $user_id,
            ]);

        return $cards;
    }

    public function saveCard($user_id, $redsys_data)
    {
        $year = substr($redsys_data['Ds_ExpiryDate'], 0, 2);
        $month = substr($redsys_data['Ds_ExpiryDate'], 2, 2);
        $entity = $this->newEntity([
            'user_id' => $user_id,
            'card_id' => $redsys_data['Ds_Merchant_Identifier'],
            'last4' => $redsys_data['Ds_CardNumber'],
            'exp_year' => $year,
            'exp_month' => $month,
            'brand' => self::$brands[$redsys_data['Ds_Card_Brand']],
          ]);


        return $this->save($entity);
    }
}
