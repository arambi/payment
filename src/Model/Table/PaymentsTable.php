<?php
namespace Payment\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use ArrayObject;
use Cake\I18n\I18n;

/**
 * Payments Model
 *
 * @method \Payment\Model\Entity\Payment get($primaryKey, $options = [])
 * @method \Payment\Model\Entity\Payment newEntity($data = null, array $options = [])
 * @method \Payment\Model\Entity\Payment[] newEntities(array $data, array $options = [])
 * @method \Payment\Model\Entity\Payment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Payment\Model\Entity\Payment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Payment\Model\Entity\Payment[] patchEntities($entities, array $data, array $options = [])
 * @method \Payment\Model\Entity\Payment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PaymentsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('payments');
    $this->displayField('id');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Jsonable', [
      'fields' => [
        'request',
        'response'
      ]
    ]);
    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'order_id' => __d( 'admin', 'Order Id'),
        'model' => __d( 'admin', 'Model'),
        'provider' => __d( 'admin', 'Provider'),
        'amount' => __d( 'admin', 'Total'),
        
      ])
      ->addIndex( 'index', [
        'fields' => [
          'order_id',
          'model',
          'provider',
          'amount',
    
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Pagos'),
        'plural' => __d( 'admin', 'Pagos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                'order_id',
                'model',
                'provider',
                'amount',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
      
  }

  public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
  {
    if( $entity->isNew())
    {
      $entity->set( 'locale', I18n::locale());
    }
  }

  public function markSent( $payment)
  {
    $payment->set( 'sent_at', date( 'Y-m-d H:i:s'));
    $this->save( $payment);
  }

  public function markSuccess( $payment, $post)
  {
    $payment->set( 'success', true);
    $payment->set( 'response', $post);
    $payment->set( 'response_at', date( 'Y-m-d H:i:s'));
    $this->save( $payment);
  }

  public function markError( $payment, $post)
  {
    $payment->set( 'success', false);
    $payment->set( 'response', $post);
    $payment->set( 'response_at', date( 'Y-m-d H:i:s'));
    $this->save( $payment);
  }
}
