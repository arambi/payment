<?php
namespace Payment\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class CardStripeOrdersTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('payment_cards_stripe_orders');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');

    $this->addBehavior('Timestamp');

  }

   
}
