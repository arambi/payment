<?php

namespace Payment\Controller;

use stdClass;
use Cake\Log\Log;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Payment\Driver\Redsys;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Payment\Controller\AppController;
use Cake\Http\Client as HttpClient;

/**
 * Redsys Controller
 *
 * @property \Payment\Model\Table\RedsysTable $Redsys
 */
class RedsysRestController extends AppController
{
    public function form()
    {
        $this->setVersion();
        $payment = $this->getPayment();
        $Orders = TableRegistry::getTableLocator()->get($payment->model);
        $order = $Orders->findById($payment->order_id)->first();
        $obOrder = new \stdClass();
        $obOrder->order_id = $order->payment_order_id;
        $event = new Event('Payment.Controller.RedsysRest.getOrderId', $this, [
            $obOrder,
            $payment
        ]);

        EventManager::instance()->dispatch($event);

        $url = Router::url([
            'plugin' => 'Payment',
            'controller' => 'RedsysRest',
            'action' => 'validate',
            'salt' => $payment->salt,
        ], true);

        $this->set(compact('url'));
    }

    private function setVersion()
    {
        $version = $this->request->getQuery('emv.protocolVersion');
        $this->set('version', substr($version, 0, 1));
    }

    public function validate()
    {
        $payment = $this->getPayment();
        $Orders = TableRegistry::getTableLocator()->get($payment->model);
        $order = $Orders->findById($payment->order_id)->first();
        $obOrder = new \stdClass();
        $obOrder->order_id = $order->payment_order_id;
        $event = new Event('Payment.Controller.RedsysRest.getOrderId', $this, [
            $obOrder,
            $payment
        ]);

        EventManager::instance()->dispatch($event);

        $data = [];

        $data['DS_MERCHANT_EMV3DS'] = [
            'threeDSInfo' => 'ChallengeResponse',
            'protocolVersion' => $order->metadata['protocolVersion'],
        ];

        if ($this->request->getData('cres')) {
            $data['DS_MERCHANT_EMV3DS']['cres'] = $this->request->getData('cres');
        } else {
            $data['DS_MERCHANT_EMV3DS']['PARes'] = $this->request->getData('PaRes');
            $data['DS_MERCHANT_EMV3DS']['MD'] = $this->request->getData('MD');
        }

        $response = $this->doRequest($this->getTrataUrl(), $payment, $obOrder, $order, $data, 'Trata Petición 2');

        if ($this->isResponseSuccess($response)) {
            \Cake\Log\Log::debug( 'Response success');
            $this->Payments->markSuccess($payment, $response);
            $callback = Configure::read('Payment.callbacks.' . $payment->callback);
            $request = $this->request->withAttribute('redsysResponse', $response);
            $this->updateOrderId($response, $payment, $order);
            $callback($payment, true, $request);
            $this->redirect([
                'plugin' => 'Store',
                'controller' => 'Orders',
                'action' => 'success',
                'cart' => $order->salt,
            ]);
        } else {
            \Cake\Log\Log::debug( 'Response failed');
            $callback = Configure::read('Payment.callbacks.' . $payment->callback);
            $callback($payment, false, $this->request);
            $this->Payments->markError($payment, $response);

            $this->redirect([
                'plugin' => 'Store',
                'controller' => 'Orders',
                'action' => 'error',
                'cart' => $order->salt,
            ]);
        }
    }

    private function updateOrderId($response, $payment, $order)
    {
        $Orders = TableRegistry::getTableLocator()->get($payment->model);
        $Orders->query()->update()
            ->set([
                'payment_order_id' => $response['Ds_Order']
            ])
            ->where([
                'id' => $order->id
            ])
            ->execute();
    }

    private function getTrataUrl()
    {
        if (Configure::read('Payment.env') == 'development') {
            return 'https://sis-t.redsys.es:25443/sis/rest/trataPeticionREST';
        } else {
            return 'https://sis.redsys.es/sis/rest/trataPeticionREST';
        }
    }


    private function isResponseSuccess($response)
    {
        return @$response['Ds_Response'] == '0000';
    }


    private function doRequest($url, $payment, $obOrder, $order, $data, $name)
    {
        $account = $this->getAccount($payment);

        $defaults = [
            'DS_MERCHANT_AMOUNT' => (string)(int)round($payment->amount * 100),
            'DS_MERCHANT_ORDER' => $obOrder->order_id,
            'DS_MERCHANT_MERCHANTCODE' => $account['MerchantCode'],
            'DS_MERCHANT_CURRENCY' => $account['Merchant_Currency'],
            'DS_MERCHANT_TRANSACTIONTYPE' => '0',
            'DS_MERCHANT_TERMINAL' => $account['Merchant_Terminal'],
        ];

        $data['DS_MERCHANT_IDENTIFIER'] = $this->getIdentifier($obOrder);
        $this->setIdOper($data, $obOrder, $order);

        $data = array_merge($defaults, $data);

        \Cake\Log\Log::debug("---- $name REQUEST ----", ['scope' => ['payments']]);
        \Cake\Log\Log::debug($data, ['scope' => ['payments']]);
        $Redsys = new Redsys();

        foreach ($data as $key => $value) {
            $Redsys->setParameter($key, $value);
        }

        $params = $Redsys->createMerchantParameters();
        $signature = $Redsys->createMerchantSignature($this->clave($payment));

        $version = "HMAC_SHA256_V1";

        $_data = [
            'Ds_SignatureVersion' => $version,
            'Ds_MerchantParameters' => $params,
            'Ds_Signature' => $signature
        ];

        $http = new HttpClient();
        $response = $http->post($url, $_data);
        $code_response = json_decode($response->getBody()->getContents(), true);
        \Cake\Log\Log::debug($code_response, ['scope' => ['payments']]);
        $data_response = $Redsys->decodeMerchantParameters($code_response['Ds_MerchantParameters']);
        $return = json_decode($data_response, true);
        \Cake\Log\Log::debug("---- $name RESPONSE ----", ['scope' => ['payments']]);
        \Cake\Log\Log::debug($return, ['scope' => ['payments']]);
        return $return;
    }

    private function setIdOper(&$data, $obOrder, $order)
    {
        $identifier = $this->getIdentifier($obOrder);
        if ($order->card_salt && $identifier == 'REQUIRED') {
            $data['DS_MERCHANT_IDOPER'] = $order->card_salt;
        }
    }

    private function getIdentifier($obOrder)
    {
        if (!empty($obOrder->card_id)) {
            $card = TableRegistry::getTableLocator()->get('Payment.RedsysCards')->findById($obOrder->card_id)->first();

            if ($card) {
                return $card->card_id;
            }
        }

        return 'REQUIRED';
    }


    public function clave($payment)
    {
        $account = $this->getAccount($payment);
        return $account[Configure::read('Payment.env')]['clave'];
    }

    private function getAccount($payment)
    {
        if (empty($payment->provider_account)) {
            return Configure::read('Payment.sermepa.data');
        }

        return Configure::read('Payment.sermepa.' . $payment->provider_account);
    }
}
