<?php
namespace Payment\Controller;

use Payment\Controller\AppController;
use Cake\Core\Configure;
use Cake\Routing\Router;

use PayPal\Api\Payment; 
use PayPal\Api\PaymentExecution; 
use Payment\Driver\PaypalTrait;
use PayPal\Api\Transaction;
use PayPal\Api\Amount;
/**
 * Redsys Controller
 *
 * @property \Payment\Model\Table\RedsysTable $Redsys
 */
class PaypalController extends AppController
{
  use PaypalTrait;
  
  public function index()
  {

  }


  public function proccess() 
  {   
    $this->loadModel( 'Payment.Payments');
    \Cake\Log\Log::debug( $this->request->getQuery());
    
    if( !isset( $this->request->query ['paymentId'], $this->request->query ['PayerID']))
    {
      $this->Section->notFound();
    }
    
    $paymentId = $this->request->query ['paymentId'];
    $payerId = $this->request->query ['PayerID'];
    $paypal = $this->apiContext();
    $payment = Payment::get( $paymentId, $paypal);
    
    $execute = new PaymentExecution();
    $execute->setPayerId( $payerId);

    $entity = $this->Payments->find()
      ->where([
        'transaction_id' => $paymentId
      ])
      ->first();
    
    if( !$entity)
    {
      $this->Section->notFound();
    }
    
    $urls = $entity->request;

    try {
      $result = $payment->execute( $execute, $paypal);
      \Cake\Log\Log::debug( $result);
      $transaction = $result->getTransactions()[0];
      $resource = $transaction->getRelatedResources()[0];
      $sale = $resource->getSale();
      $state = $sale->getState();

      if( $result->getState() == 'approved' && in_array( $state, ["completed", "pending"]))
      {
        $entity->set( 'transaction_id', $sale->getId());
        $this->Payments->markSuccess( $entity, $_POST);

        $callback = Configure::read( 'Payment.callbacks.'. $entity->callback);
        $status = $state == "completed" ? 'accepted' : 'pending';
        $callback( $entity, true, $this->request, $status);
        $this->redirect( $urls->success_url);
      }
      else
      {
        $this->setError( $entity, $urls);
      }

    } catch (\Exception $e) {
      \Cake\Log\Log::debug( $e->getMessage());
      $this->setError( $entity, $urls);
    }

  }

  private function setError( $entity, $urls)
  {
    $callback = Configure::read( 'Payment.callbacks.'. $entity->callback);
    $callback( $entity, false, $this->request);
    $this->Payments->markError( $entity, $_POST);
    $this->redirect( $urls->error_url);
  }

  public function clave()
  {
    return Configure::read( 'Payment.paypal.data.'. Configure::read('Payment.env') .'.clave');
  }  
}
