<?php

namespace Payment\Controller;

use stdClass;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Payment\Driver\Redsys;
use Cake\Event\EventManager;
use Payment\Controller\AppController;

/**
 * Redsys Controller
 *
 * @property \Payment\Model\Table\RedsysTable $Redsys
 */
class RedsysController extends AppController
{
  public function index()
  {
    $payment = $this->getPayment();

    $obOrder = new \stdClass();
    $obOrder->order_id = str_pad($payment->id, 6, '0', STR_PAD_LEFT);
    $event = new Event('Payment.Controller.Redsys.getOrderId', $this, [
      $obOrder,
    ]);

    EventManager::instance()->dispatch($event);

    $account = $this->getAccount($payment, $obOrder->order_id);

    $Redsys = new Redsys();
    $currency = is_callable($account['Merchant_Currency']) ? $account['Merchant_Currency']($obOrder->order_id) : $account['Merchant_Currency'];
    $Redsys->setParameter("DS_MERCHANT_AMOUNT", (int)round($payment->amount * 100));
    $Redsys->setParameter("DS_MERCHANT_ORDER", $obOrder->order_id);
    $Redsys->setParameter("DS_MERCHANT_MERCHANTCODE", $account['MerchantCode']);
    $Redsys->setParameter("DS_MERCHANT_CURRENCY", $currency);
    $Redsys->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $account['Merchant_TransactionType']);
    $Redsys->setParameter("DS_MERCHANT_TERMINAL", $account['Merchant_Terminal']);
    $Redsys->setParameter("DS_MERCHANT_MERCHANTURL", Router::url('/payment/redsys/proccess', true));
    $Redsys->setParameter("DS_MERCHANT_URLOK", $payment->request->success_url);
    $Redsys->setParameter("DS_MERCHANT_URLKO", $payment->request->error_url);
    $Redsys->setParameter("DS_MERCHANT_NAME", $account['Merchant_MerchantName']);
    $Redsys->setParameter("DS_MERCHANT_CONSUMERLANGUAGE", $account['Merchant_ConsumerLanguage']);
    
    if (!empty($payment->titular)) {
      $Redsys->setParameter("DS_MERCHANT_TITULAR", $payment->titular);
    }
    
    $params = $Redsys->createMerchantParameters();
    $signature = $Redsys->createMerchantSignature($this->clave($payment, $obOrder->order_id));

    // _d( $signature);
    if (Configure::read('Payment.env') == 'development') {
      $data['form_action'] = 'https://sis-t.redsys.es:25443/sis/realizarPago';
    } else {
      $data['form_action'] = 'https://sis.redsys.es/sis/realizarPago';
    }

    $version = "HMAC_SHA256_V1";
    $data['Ds_SignatureVersion'] = $version;
    $data['Ds_MerchantParameters'] = $params;
    $data['Ds_Signature'] = $signature;
    $this->set(compact('data'));

    $this->Payments->markSent($payment);
  }

  public function validate($payment)
  {
    $Redsys = new Redsys();

    if (empty($_POST)) {
      return false;
    }

    $version = $_POST["Ds_SignatureVersion"];
    $datos = $_POST["Ds_MerchantParameters"];
    $signatureRecibida = $_POST["Ds_Signature"];

    $data = $Redsys->decodeMerchantParameters($_POST['Ds_MerchantParameters']);

    if (empty($data)) {
      return false;
    }

    $data = json_decode($data, true);
    $response = (int)$data['Ds_Response'];
    $validate = $data['Ds_Response'] == '0000' || ($response > 1 && $response < 100);

    $decodec = $Redsys->decodeMerchantParameters($datos);
    $firma = $Redsys->createMerchantSignatureNotif($this->clave($payment), $datos);
    return $validate && $firma === $signatureRecibida;
  }

  public function proccess()
  {
    $Redsys = new Redsys();

    $data = $Redsys->decodeMerchantParameters($_POST['Ds_MerchantParameters']);
    $data = json_decode($data, true);
    $this->loadModel('Payment.Payments');

    $obOrder = new \stdClass();
    $obOrder->order_id = $data['Ds_Order'];
    $event = new Event('Payment.Controller.Redsys.setOrderId', $this, [
      $obOrder,
    ]);

    EventManager::instance()->dispatch($event);

    $payment = $this->Payments->find()
      ->where([
        'Payments.id' => $obOrder->order_id
      ])
      ->first();

    if ($this->validate($payment)) {
      $this->Payments->markSuccess($payment, $_POST);
      $callback = Configure::read('Payment.callbacks.' . $payment->callback);
      $callback($payment, true, $this->request);
    } else {
      $callback = Configure::read('Payment.callbacks.' . $payment->callback);
      $callback($payment, false, $this->request);
      $this->Payments->markError($payment, $_POST);
    }

    $this->render('response', 'ajax');
  }

  public function clave($payment, $order_id = null)
  {
    $account = $this->getAccount($payment, $order_id);
    return $account[Configure::read('Payment.env')]['clave'];
  }

  private function getAccount($payment, $order_id = null)
  {
    if (empty($payment->provider_account)) {
      $data = Configure::read('Payment.sermepa.data');
    } else {
      $data = Configure::read('Payment.sermepa.' . $payment->provider_account);
    }

    if (!$order_id) {
      $order_id = $payment->id;
    }

    if (is_callable($data) && $order_id) {
      return $data($order_id);
    }

    return $data;
  }
}
