<?php
namespace Payment\Controller;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Payment\Controller\AppController;

/**
 * Stripe Controller
 *
 *
 * @method \Payment\Model\Entity\Stripe[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StripeController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
    $this->loadComponent('Store.Cart');

    if( isset( $this->Auth))
    {
      $this->Auth->allow();
    }
  }

  public function updateIntent()
  {
    $order = $this->Cart->order();

    if( !$order)
    {
      return;
    }

    $this->loadModel( 'Payment.CardStripeOrders');

    $entity = $this->CardStripeOrders->find()
      ->where([
        'order_id' => $order->id,
        'model' => 'Store.Orders',
      ])
      ->first();
    
    \Stripe\Stripe::setApiKey( $this->getKeys()['secret']);
    $intent = \Stripe\PaymentIntent::retrieve( $entity->intent_id);
    
    if( $intent->status != 'succeeded') {
      \Stripe\PaymentIntent::update(
        $entity->intent_id,
        [
          'amount' => round( $order->total * 100),
        ]
      );
    }

    $this->set( 'intent', $intent);
    
    $this->set( '_serialize', true);
  }

  private function getKeys()
  {
    $prefix = Configure::read( 'App.payEnvironment') == 'development' ? 'test_' : 'live_';

    $payment = TableRegistry::get( 'Store.PaymentMethods')->find()
      ->where([
        'PaymentMethods.processor' => 'stripe'
      ])
      ->first();
    
    
    if( !$payment || empty( $payment->settings->{$prefix . 'public'}) || empty( $payment->settings->{$prefix . 'secret'}))
    {
      throw new \RuntimeException(
          'No está configurado Stripe'
      );
    }

    return [
      'public' => $payment->settings->{$prefix . 'public'},
      'secret' => $payment->settings->{$prefix . 'secret'},
    ];
  }
}
