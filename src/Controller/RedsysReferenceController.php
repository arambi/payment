<?php

namespace Payment\Controller;

use stdClass;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Payment\Driver\Redsys;
use Cake\Event\EventManager;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Payment\Controller\AppController;

/**
 * Redsys Controller
 *
 * @property \Payment\Model\Table\RedsysTable $Redsys
 */
class RedsysReferenceController extends AppController
{
    public function index()
    {
        $payment = $this->getPayment();
        $account = $this->getAccount($payment);

        $obOrder = new \stdClass();
        $obOrder->order_id = str_pad($payment->id, 6, '0', STR_PAD_LEFT);
        $event = new Event('Payment.Controller.RedsysReference.getOrderId', $this, [
            $obOrder,
            $payment
        ]);

        EventManager::instance()->dispatch($event);

        $Redsys = new Redsys();
        $Redsys->setParameter("DS_MERCHANT_AMOUNT", (int)round($payment->amount * 100));
        $Redsys->setParameter("DS_MERCHANT_ORDER", $obOrder->order_id);
        $Redsys->setParameter("DS_MERCHANT_MERCHANTCODE", $account['MerchantCode']);
        $Redsys->setParameter("DS_MERCHANT_CURRENCY", $account['Merchant_Currency']);
        $Redsys->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $account['Merchant_TransactionType']);
        $Redsys->setParameter("DS_MERCHANT_TERMINAL", $account['Merchant_Terminal']);
        $Redsys->setParameter("DS_MERCHANT_MERCHANTURL", Router::url('/payment/redsys-reference/proccess', true));
        $Redsys->setParameter("DS_MERCHANT_URLOK", $payment->request->success_url);
        $Redsys->setParameter("DS_MERCHANT_URLKO", $payment->request->error_url);
        $Redsys->setParameter("DS_MERCHANT_NAME", $account['Merchant_MerchantName']);
        $Redsys->setParameter("DS_MERCHANT_CONSUMERLANGUAGE", $account['Merchant_ConsumerLanguage']);
        $Redsys->setParameter("DS_MERCHANT_IDENTIFIER", 'REQUIRED');

        \Cake\Log\Log::debug( '------------------ REDSYS');
        \Cake\Log\Log::debug( $obOrder);
        if (!empty($obOrder->card_id)) {
            $card = TableRegistry::getTableLocator()->get('Payment.RedsysCards')->findById($obOrder->card_id)->first();

            if ($card) {
                $Redsys->setParameter("DS_MERCHANT_IDENTIFIER", $card->card_id);
            }
        }


        if (!empty($payment->titular)) {
            $Redsys->setParameter("DS_MERCHANT_TITULAR", $payment->titular);
        }

        $params = $Redsys->createMerchantParameters();
        $signature = $Redsys->createMerchantSignature($this->clave($payment));

        // _d( $signature);
        if (Configure::read('Payment.env') == 'development') {
            $data['form_action'] = 'https://sis-t.redsys.es:25443/sis/realizarPago';
        } else {
            $data['form_action'] = 'https://sis.redsys.es/sis/realizarPago';
        }

        $version = "HMAC_SHA256_V1";
        $data['Ds_SignatureVersion'] = $version;
        $data['Ds_MerchantParameters'] = $params;
        $data['Ds_Signature'] = $signature;
        $this->set(compact('data'));

        $this->Payments->markSent($payment);
    }

    public function validate($payment)
    {
        $Redsys = new Redsys();

        if (empty($_POST)) {
            return false;
        }

        $version = $_POST["Ds_SignatureVersion"];
        $datos = $_POST["Ds_MerchantParameters"];
        $signatureRecibida = $_POST["Ds_Signature"];

        $data = $Redsys->decodeMerchantParameters($_POST['Ds_MerchantParameters']);

        if (empty($data)) {
            return false;
        }

        $data = json_decode($data, true);
        $response = (int)$data['Ds_Response'];
        $validate = $data['Ds_Response'] == '0000' || ($response > 1 && $response < 100);

        $decodec = $Redsys->decodeMerchantParameters($datos);
        $firma = $Redsys->createMerchantSignatureNotif($this->clave($payment), $datos);
        return $validate && $firma === $signatureRecibida;
    }

    public function proccess()
    {
        $Redsys = new Redsys();

        $data = $Redsys->decodeMerchantParameters($_POST['Ds_MerchantParameters']);
        $data = json_decode($data, true);
        \Cake\Log\Log::debug($data);
        $this->loadModel('Payment.Payments');

        $obOrder = new \stdClass();
        $obOrder->order_id = $data['Ds_Order'];
        $event = new Event('Payment.Controller.Redsys.setOrderId', $this, [
            $obOrder,
        ]);

        EventManager::instance()->dispatch($event);

        $payment = $this->Payments->find()
            ->where([
                'Payments.id' => $obOrder->order_id
            ])
            ->first();

        if ($this->validate($payment)) {
            $this->Payments->markSuccess($payment, $_POST);
            $callback = Configure::read('Payment.callbacks.' . $payment->callback);
            $callback($payment, true, $this->request);
        } else {
            $callback = Configure::read('Payment.callbacks.' . $payment->callback);
            $callback($payment, false, $this->request);
            $this->Payments->markError($payment, $_POST);
        }

        $this->render('response', 'ajax');
    }

    public function clave($payment)
    {
        $account = $this->getAccount($payment);
        return $account[Configure::read('Payment.env')]['clave'];
    }

    private function getAccount($payment)
    {
        if (empty($payment->provider_account)) {
            return Configure::read('Payment.sermepa.data');
        }

        return Configure::read('Payment.sermepa.' . $payment->provider_account);
    }
}
