<?php

namespace Payment\Controller;

use App\Controller\AppController as BaseController;
use Cake\Core\Configure;
use Cake\Routing\Router;

class AppController extends BaseController
{
  public function initialize()
  {
    parent::initialize();

    if( isset( $this->Auth))
    {
      $this->Auth->allow();
    }

    $this->viewBuilder()->templatePath( 'general');
  }

  public function getPayment() 
  {
    if( $this->request->getQuery( 'salt'))
    {
      $this->loadModel( 'Payment.Payments');
      $payment = $this->Payments->find()
        ->where([
          'Payments.salt' => $this->request->getQuery( 'salt')
        ])
        ->first();
      
      return $payment;
    }
    
    if( $this->request->session()->check( 'Payment.id'))
    {
      $this->loadModel( 'Payment.Payments');
      $payment = $this->Payments->find()
        ->where([
          'Payments.id' => $this->request->session()->read( 'Payment.id')
        ])
        ->first();

      return $payment;
    }
  } 

}
